# Network Paths Groups

## Specification

```
network_paths_group:
  <network_path_group_name>:
    - <network_path_name_1>
    - <network_path_name_N>
```

## References

`network_paths_group`:
```
  type: "dictionnary"
  description: "List of Group of network paths for fast reference in services."
```

`network_paths_groups.<network_paths_group_name>`:
```
  type: "list"
  example:
    openshift_network_path_group:
      - cri_ens-to-openshift
      - cri_ung-to-openshift
      - sia_prod-to-openshift
      - sia_dms-to-openshift
  This network_path_group allows paths from multiple interfaces of the router to be referenced in on go.
```
