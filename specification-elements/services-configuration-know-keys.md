# Services configuration known keys

## Specification

```
url:
  scheme: "scheme"
  hostname: "main_hostname"
  alternative_hostname: "alternative_hostname" # Only makes sense as a redirection
  port: # Default from protocol
```

## References

`services.<service_name>.url`:
```
  type: "dictionnary"
  mandatory: "yes"
  description: "describes the URL used to access the service."
```

`services.<service_name>.url.scheme`:
```
  type: "string"
  mandatory: "no, defaults to 'HTTPS'."
  example: "'HTTPS', 'SSH', 'LDAPS', ..."
  description: protocol (HTTPS, LDAPS, ...) used."
```

`services.<service_name>.url.hostname`:
```
  type: "string"
  mandatory: "yes"
  description: "Hostname to access the service."
  example: "google.com"
  thinking: "Some precautions should be taken with certificates for alternative hostnames. Should monitoring be configured per hostname ?"
```

`services.<service_name>.url.alternative_hostname`:
```
  type: "string"
  mandatory: "no"
  description: "Alternative hostname to access the service. Depending on the scheme, this should configure HTTP redirects or DNS CNAME.  example: "google.com"
  usage: "For discovery, for exemple redirecting ldaps.exemple.com to freeipa.exemple.com. Some precautions should be taken with certificates for alternative hostnames."

```

`services.<service_name>.url.port`:
```
  type: "string"
  mandatory: "no, default based on scheme."
  description: "Port to access the service."
  example: "'443' for HTTPS"
  thinking: "Could be in a DNS SRV record. Is it the port on node hosting the service, or the port to access it from anywhere else ? For example a webserver could be hosted on port 8080, but through a proxy is accessible using port 80"
  note: "This is still WIP"
```
