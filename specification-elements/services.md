# Services

## Specification

```
services:
  <service-name>:
    execution_environment: <execution_environment_name>

    deployment_parameters:
      <parameter_1>: {}
      <parameter_2>: {}

    configuration:
      <config_key_1>: {}
      <config_key_2>: {}
      # See also : services-configuration-known-keys
      ...

    network:
      entryPoints:

    network_paths:
      - <network_path_name> or <network_path_group_name>
```

## References

`services`:
```
  type: "dictionnary"
  description: "List of services"
```

`services.<service_name>.execution_environment`:
```
  type: "string"
  mandatory: "yes"
  description: "Name of the execution environment"
  example: "docker, systemd-service, package, kubernetes, ..."
```
Related problematic: `Service deployment`

`services.<service_name>.deployment_parameters`:
```
  type: "dictionnary"
  mandatory: "yes"
  description: "Details to deploy the service in the execution environment"
  example: "docker image name, url to recover the binary, folder to put the binary, ..."
  note: "a specification could evolve for each execution environment"
```
Related problematic: `Service deployment`

`services.<service_name>.configuration`:
```
  type: "dictionnary"
  mandatory: "no"
  description: "List of configuration variables passed as CLI arguments or to generate config files"
```
Related problematic: `Service configuration`

`services.<service_name>.network_paths`:
```
  type: "list"
  mandatory: "yes"
  description: "List of `network_paths`. Those are defined in the `network_paths` global dictionnary."
```
Related problematic: `Network Configuration`

