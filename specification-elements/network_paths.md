# Network Paths

## Specification

```
network_paths:
  <network_path_name>:
    - <network_node_1>: {} # initial_network_host = entrypoint in your network
    - <network_node_2>: {} # intermediate_network_host = host modifying the trafic
    - <network_node_3>: {} # intermediate_network_host
    - <network_node_N>: {} # destination_network_host = last host configured from this specification. Ideally the target host/cluster where the service is running.
```

## References

`network_paths`
```
  type: "dictionnary"
  description: "Contains all the `network_paths` referenced in `services`. A network path list the network nodes interacting with the datastream and modifying it. This can be a router with NAT/PAT, a loadbalancer, a reverse-proxy, ..."
```

`network_paths.<network_path_name>`
```
  type: "dictionnary"
  description: "A specific network_path. Can contain any number of entry. The first entry will be the `initial_network_host`, the last entry the `destination_network_host` and any entry in-between are `intermediate_network_host`."
  thinking: "Sometimes, the last entry is not the `destination_network_host` because the network parts after don't need to be referenced. One good example would be a kubernetes cluster, that include an ingress controller (=reverse-proxy) and services (=loadbalancer)."
```

`network_paths.<network_path_name>.<network_node>`:
```
  type: "dictionnary"
  description: "A `<network_node>` should match host/group from ansible. This allows to load related variables. This is a dictionnary so that you can detail some variables specific to the `network_path` (ex: interface in a router."
```

The final network_host can be a group vip.

Current thinking hasn't yet extended to:
  - multiple initial_network_hosts (for exemple multihoming or external loadbalancer)
  - loadbalancer not last or second-to-last network, because this would involve groups
