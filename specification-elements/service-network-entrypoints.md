# Network Entrypoints

## Specification

```
entryPoints:
  <entryPoint_name>:
    ipv4: ""
    ipv6: ""
    port: ""
    protocol: ""

```

## Reference

`services.<service_name>.network.entryPoints`:
```
  type: "list"
  mandatory: "yes"
  description: "Dictionnary of `entryPoints`. Must contain at least one entryPoint"
```
Related problematic: `Declaring service entrypoints`

`services.<service_name>.network.entryPoints.<entryPoint_name>`:
```
  type: "dictionary"
  mandatory: "atLeastOne"
  description: "Placeholder for an entrypoint details"
```
Related problematic: `Declaring service entrypoints`

`services.<service_name>.network.entryPoints.<entryPoint_name>.ipv4`:
```
  type: "string"
  mandatory: "no"
  description: "IPv4 Address where this service listen. Is mandatory except if specifying `ipv6`"
```
Related problematic: `Declaring service entrypoints`

`services.<service_name>.network.entryPoints.<entryPoint_name>.ipv6`:
```
  type: "string"
  mandatory: "no"
  description: "IPv6 Address where this service listen. Is mandatory except if specifying `ipv4`"
```
Related problematic: `Declaring service entrypoints`

`services.<service_name>.network.entryPoints.<entryPoint_name>.port`:
```
  type: "string"
  mandatory: "yes"
  description: "UDP or TCP port"
```
Related problematic: `Declaring service entrypoints`

`services.<service_name>.network.entryPoints.<entryPoint_name>.protocol`:
```
  type: "string"
  mandatory: "yes"
  description: "Protocol that needs to be used"
```
Related problematic: `Declaring service entrypoints`
