# Contributing

## Enriching the specification

To allow futur contributors to improve on the specification, they need to understand why the current specification is this way. To permit that, every modification of the specification MUST resolve a problematic. This means before modifying the specification, you MUST write the problematic you are trying to resolve.

To understand why a key exists and why it was designed that way, you MUST to link a key to its related problematics.

To allow knowing how a problematic was resolved, you MUST link a problematic to the related keys.

## Versionning

Each time you change a specification part, you MUST increase the specification version in the [specification](specification.md).

This is necessary because the specification is bound to evolve, and use cases won't be updated as often as the specification.

## Convention

As in kubernetes, specification keys MUST be camelCase

