% Services Infrastructure Declaration Format Specification

## Format

To ease work on the specification, parts of the specification are defined in the [specification elements](specification-elements/). Ultimately a tool should be used to build the specification into a single file. But as this is a Work In Progress, this is not urgent.

## Specification

Current version: 0.2

- [services](specification-elements/services.md#Specification)
- [network_paths](specification-elements/network_paths.md#Specification)
- [network_paths_groups](specification-elements/network_paths_groups.md#Specification)
