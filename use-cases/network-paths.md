# Network devices responsabilities and required data in SIA current infrastructure

This was used to think about what `network_paths` should contain. This used version 0.1

## haproxy-external
Elements :
- Frontend
  - Listen: ip+port+parameters
  - ACL: acl_name to haproxy regex
  - use_backend : link acl to backend
- Backend

Responsability :
  - Build ACL
  - In frontend declaration: Link ACL to backend in `use_backend`

Required data :
- frontend_name:
  - Declaration method: From `haproxy-external.frontend`
  - Generated method: Frontend name generated from `scheme`, use <service>.url.scheme
- backend_name:
  - Declaration method: From `haproxy-external.backend`
  - Generated method: Backend name generated from destinations declaration.
- acl_regex: From `services.<service_name>.url.hostname`
- acl_name: Generate from `acl_regex`

Result : need for a way to configure the device from the network path
Related problematic : `Configure network elements for path deployment`

## Network - Expected and Configurable

Working Ethernet
  Configure VLAN

IP Network
  DHCP
  Static routes ?

Execution Environment IP Adress
  Configure Service IP Adress different from execution environment
  Request static IP

Routing
  Configure NAT / Port Forward

Loadbalancing / Reverse Proxy
  Configure rules & backends

DNS

## Service entrypoint

/home/jdorel/documents/areas/hobbies/foss-dev/documentation-repositories
Entrypoint details : IP / Port / URL ex
Default port = same as service port

Entrypoint = how the execution environment exposes the service. Some internal network functions might be in place.

Ex: a service inside a docker container has a different entrypoint than what can be seen from the service configuration because of port forward.

## Specification

```
network:
  ingress_point:
    execution_environment_interface # Linked IP adress
    port
    url
  path:
    node:
      rulename
      rule_details # If doesn't exist, rulename should use a declared route
```

Well known network: internet

## Use cases

Configure VM : VLAN, IP Configuration (IP, Mask, Gateway)

Configure Loadbalancer / Reverse Proxy

Configure NAT

Configure Firewall

Make sure path exist --> Required network routes

## Thinking elements

Network element configuration data related to services should be in the related service
Network element configuration data related to the network shouldn't be in the services section

### Configure network elements for path deployments

Shouldn't network configuration be in the network element declaration ?
  ==> No, because it provide a network support service to the end service. This helps prevent rotting configuration because when the service is decommissioned, the related rules could be removed automatically. This also mean a rule is always linked to the related services.

This can be used to generate a report matching rules with related services.

Instead of doing this :
```
network_equipments:
  main_router:
    rules:
      port-forward to host A # Comment to link to service 'website a'
```
Do that :
```
services:
  website_a:
    network_path:
      - port_forward_main_router_to_host_a

network_paths:
  port_forward_main_router_to_host_a:
    network_equipment: main_router
    rule: port-forward to host A
```

Advantages :
- force link between service and network rule
- when debuging a service, see related network_paths directly, and so related network equipments. Visibility gain
- before loading a network_path rule into the network_equipment, can check if network_path is referenced in an active service. If not, it means it's a rotten rule and skip it. Maintenance gain

## Network equipments

network_equipments:
  router_sia:
    interfaces:
      - cri_ens
      - ung

## Network_resolution

`url` and `network_paths` are both about network resolution, maybe do the following :
  - include `network_paths` in `network_resolution`
  - move everything from `url` to `network_resolution` ?
```
services:
  <service_name>:
    network_resolution
      scheme:
      hostname:
      port:
      network_paths:
```

# Evolution of network_paths

## Allow referencing subpaths
Example:

```
network_paths:
  path1:
  - loadbalancer
  - test_cluster
  path2:
  - router
    interface: interface1
  - path1
  path3:
  - router1
    interface: interface2
  - path1
```
Different example could be when some long paths are used.

## Allow referencing multiple interfaces

Example:
```
network_paths:
  from_router:
  - router:
    interfaces:
      - "interface 1"
      - "interface 2"
  - web_host
```

# network_host other types
bastion ssh is a network_host ?
identification proxy
service mesh
api gateway

Services can require the following from the network :
- access to other elements (internet, service, ...) (`egress`)
- be accessible from other elements (`endpoint`)

Network Path (routing) vs Network Access (security rules)

Exposing the service outside the execution environment
  Single endpoint
  Cluster --> Loadbalancing
