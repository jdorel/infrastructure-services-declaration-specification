# Configuring port forward

## Objective & Context

The objective is to Configure a router to implement port-forward to a service.

In this use case, we used a pfSense router.

## Required data and how to retrieve it

|----------------|---------------------------------------|---------------------------------------------------------------------------|--------------------------------|
|  Required data |                 Why ?                 |                            How to retrieve it ?                           |       Related problematic      |
|:--------------:|:-------------------------------------:|:-------------------------------------------------------------------------:|:------------------------------:|
|    Interface   |    Apply rule to specific interface   |    `network_paths.cri_ens-to-openshift.router_sia.receiving_interface`    |   Configuring a network rule   |
|    Protocol    |             Rule condition            |  `service.<service_name>.network.entryPoints.<entryPoint_name>.protocol`  | Declaring a service entrypoint |
| Source Address |             Rule condition            |                 Defaults to wildcard (only useful for PBR)                |                ?               |
|  Source Ports  |             Rule condition            |                 Defaults to wildcard (only useful for PBR)                |                ?               |
|  Dest. Address |  Rule condition: Dest IP before rule  | `services.<service_name>.network.entryPoints.<entryPoint_name>.ipAddress` | Declaring a service entrypoint |
|   Dest. Ports  | Rule condition: Dest port before rule |    `services.<service_name>.network.entryPoints.<entryPoint_name>.port`   | Declaring a service entrypoint |
|     NAT IP     |    Rule effect: Dest IP after rule    |                `network-device: haproxy-external: hostname`               |   Configuring a network rule   |
|    NAT Ports   |   Rule effect: Dest Port after rule   |                  `network-device: haproxy-external: port`                 |   Configuring a network rule   |
|----------------|---------------------------------------|---------------------------------------------------------------------------|--------------------------------|

## Example declaration

/!\ UNFINISHED WORK - WORKING ON RELATED PROBLEMATICS

```
version: 0.0.2

services:
  web_service:
    execution_environment: ...

    network:
      entryPoints:
        web:
          ipv4: 192.168.0.10
          port: 80
          protocol: http

    network_path:
      pfsense_router
        receiving_interface:
        network_function: port_forward # To known which parameters to pass for the rule
          #
```
