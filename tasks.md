# Infrastructure Declaration Specification - Tasks

## Doing

Use case : configuring port forward
  Problematic : Declaring a service entrypoint
    Issue : declare URL instead of TCP/IP
    URL --> Access the service
    entryPoint --> Configure network

## TODO

Merge execution_environment and deployment_parameters -->
```
deployment:
  environment:
  parameters:
```

Migrate to camelCase

Problematics
- Configuring a network rule

- move execution environment reference exemple in dedicated file
- link resolved problematics to keys --> Make CONTRIBUTING.md
- merge design-process into problematics
- resolve network_path problematic through use-cases

Use cases to study :
- configure DNS for service directly accessible
- configure DNS for service behind reverse proxy
- configure reverse proxy
- generate and deploy letsencrypt certificates
- deploy blackbox monitoring from services URLs
- deploy whitebox monitoring through prometheus node-exporter
- deploy service
- deploy hosting plateform
- configure docker-port-forward
