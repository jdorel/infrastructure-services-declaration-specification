# Services Infrastructure Declaration Format Specification

This is an attempt to build a standard to declare an infrastructure.

## Why does this exists ?

The objective is to consume this specification to deploy an infrastructure. When doing so, some informations need to be shared to multiple systems (for exemple, the URL of a service is consumed to configure the service and the reverse proxy). Instead of repeating those informations across multiple locations, it should be declared once in your infrastructure declaration and use by your playbooks.

Having a standard mean the community can use this to build roles. This spares you from having to declare your infrastructure the way the role designer decided to, not matching the format from another role.

<!-- If you still don't understand why this is useful, see example [use cases](use-cases.md). -->

It could also be used to produce a view of the infrastructure to make it more accessible for newcomers.

## How was it designed ?

The root of this specification are the services, this approach allows infrastructure administrators to focus on what's important : providing services to users or machines.

From there, the specification was built by studying [use cases](use-cases/) : which data is required to deploy which system, and how should this data be represented in this standard ? This last questions are what we called [problematics](problematics/). There are still some [unresolved problematics](problematics/unresolved-problematics.md).

## Current work

The work on this specification is ongoing. Some tasks can be found in the [tasks file](tasks.md).

## Current approach

This specification has been written while thinking about how Ansible can use the data to manipulate an infrastructure. Maybe other tooling would require another approach, but in this absolute this specification doesn't depend on any specific tooling.

## Evolution

Until version 1.0 is implemented, the specification doesn't follow semantic versionning. Meaning anything can break.

## Content

This repository contains :
* the [specification](specification.md), that still has to be generated from the [specification elements](specification-elements/)
* the [specification elements](specification-elements/), containing parts of the specification and the related references
* the [problematics](problematics.md) the specification is trying to resolve.
* the [unresolved problematics](unresolved-problematics.md) the specification has yet to solve.
* the [use cases](use-cases/) that were used to design the specification, but that can also serve as examples.

* some [example declarations](example_declarations)
