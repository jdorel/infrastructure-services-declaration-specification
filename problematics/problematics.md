% Problematics

# Service Provider

To provide a service, you need to :
- deploy the service in an `execution environment`
- configure the service
- configure the network to access the service

## Service Deployment

To execute a service, you need to deploy it inside an `execution environment`

Services can be executed in a lot of ways. For exemple :
- directly on an operating system, either baremetal or in a VM
- inside a container, either standalone or managed by an orchestrator
- as SaaS

To solve this, the specification needs three elements:
- a list of execution environments (key: `execution_environments`)
  - This is necessary because the execution environment needs to exist before the service can be deployed. Informations in this list can also be used for provisionning.
- a link between the service and the execution environment (key: `services.<service_name>.execution_environment`)
- details about how the service is implemented in the execution environment (key: `services.<service_name>.deployment_parameters`)
  - For example, to execute a service as a docker container, you need to specify the image, the volumes, ... either directly or through a file with those informations

## Service Configuration

Depending on the service, configuring the service can be done through a file, through CLI arguments or through environment variables.

You might want to put in your configuration a detail that needs to be reused by another part of your infrastructure (ex: url for monitoring) or that comes from another part of your infrastructure (ex: credential to access authentication service). Hence you need a list of variables used for this service configuration (key: `services.<service_name>.configuration`)

These variables can then be passed as CLI arguments or environment variables to the `execution environment`, or in templates to generate configuration files.

Because some configuration elements are so generic, it makes sense to standardize their declaration to prevent everyone from declaring it in differents ways.

## Network Configuration

Once the service is up and running, chances are you want it to be accessible from outside. If your service is consumed localy, this might not be necessary.

Turns out though, declaring the relation between a service and the network is hard. To solve this problematic, we designed the solution by thinking about use cases.

### Declaring a service entrypoint

To make a service accessible from the network, two things are necessary :
- configure the service to be accessible from the network
- share with other elements the required informations

Because this specification is still quite new, we will focus on services accessible through IP, using either TCP or UDP.

Hence, the following elements are required :
- `ip address` to establish the connection
- `TCP/UDP port` to establish the connection
- `protocol` to speak the same language

/!\ Sometimes, your network abstracted TCP/IP, and you just manipulates URLs

We selected the generic key `network.entryPoints` to allow extension to other type of networks in the futur (ex: Ethernet).
Because some services can use multiple entrypoints (for example a mail server), this key is a dictionnary.

Use cases :
- configure service / execution environment
- configure network devices
- documentation

Related use cases:
- configuring-port-forward
- docker-port-forward
  Use path --> node host-docker --> port-forward
- services with direct access (manipulating ethernet or other kind of networks)
- service use multiple ports / ip

### Declaring network path
### Configuring a network rule

Data used :
- Elements already declared (from service_entrypoint)
- Details to add

# Infrastructure Provisionning

As seen in the previous section, some elements are required to provide services. Those can be seen as `infrastructure services`. By declaring them, you can use the infrastructure declaration to provision those elements.

When you only consider the necessity to provide a service, you only to provision the following :
- execution_environments
- network_elements

## Execution environments

## Network

```
network_nodes:
- <node1>:
  interfaces?:

network_links;
- link_name:
    interface_1:
    interface_2:
```

# Support services

Unfortunately, running an infrastructure requires other elements :
- monitoring (metrics, logs, trace)
- backup
- authentication
- security
- high availability
- scaling

All those elements are in the `unresolved problematics`
