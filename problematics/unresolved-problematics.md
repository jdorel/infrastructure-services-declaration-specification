# Unresolved problematics

Unresolved problematics are problematics that have not yet made it into the specification. They are futur of the specification.

# Evolutions of service keys

## ACL

Qui peut accéder au service, et depuis où ?

# Evolution of global keys

New keys: jobs / cronjobs --> Type of service ?

## Get default port from scheme in ansible

defaultPorts:
  http: "80"
  https: "443"
  ldap: "389"
  ldaps: "636"
  dns: "53"

Get port : {{ item.port | default(defaultPorts[item.scheme]) | default "443" }}

## URL Security

Who can access URLs, from where

## Reuse service executation environment details between prod, dev and staging

## Support services

### Backups

```
<service_name>:
  data:
    - <volume_name>
      location
      backup_policy: <backup_policy_name>

backup_policies:
  <backup_policy_name>:
```

Policies :
  Frequency
  Full / Incremental
  Location (geographical, organisation, provider, ...)
  Replication
  Pruning


### Monitoring

Permettre de configurer le monitoring :
  external-blackbox : Configure le monitoring d'un point de vue consommateur
  internal-blackbox : Configure le monitoring d'un point de vue interne. Permet de distinguer les problèmes réseaux internes des problèmes réseaux externes.
  whitebox : Configure le monitoring depuis l'application. Si le monitoring externe indique que le service est down alors que depuis l'intérieur le site est up, cela permet de diagnostiquer rapidement si c'est un problème du service ou du réseau
  alerting:
    - <urgency>: niveau d'alertes
    - none : Désactive les alertes de certains sites (ex: les sites de dev).
  none : Par defaut, désactive le monitoring
    monitoring: listOf (internal (internal blackbox) | external (external blackbox) | none[default] )
    backup:

## Auth
Configure la méthode d'authentification au service ? Ou juste une string de documentation qui pourra être utilisé pour générer un rapport ?

### Support services targetting in services or execution environment

Use `services.<service_name>.tags` or `services.<service_name>.groups`

`groups` could be datacenters, state (prod, staging, ...)

## Security

Is security (for exemple ACL) a service ?

## Unsorted

### Port

To access a service, you sometime need to specify a port because something else than the well-known port is used. But because of network elements, the service might be published on the execution environment on one port but accessible to users through another port. For example a web server published on port 8080 but accessible to users through a reverse proxy.
Where should the port be declared ? In configuration, in network path or somewhere else ?
